use std::{error::Error, io};

use orgize::export::{DefaultHtmlHandler, HtmlHandler};
use orgize::{Element, Org};
use regex::Regex;
use slugify::slugify;

use syntect::html::{ClassStyle, ClassedHTMLGenerator};
use syntect::parsing::SyntaxSet;
use syntect::util::LinesWithEndings;

#[derive(Default)]
pub struct SkinsuitHtmlHandler(DefaultHtmlHandler);

fn process_text(the_text: &str) -> Result<String, Box<dyn Error>> {
    let all_regexes = vec![
        (r"\b[A-Z]{2,}\b", "<abbr>$0</abbr>"),
        (r"\B---\B", "&mdash;"),
        (r"\B--\B", "&ndash;"),
    ];

    let mut text = the_text.to_string();
    for (pattern, replace) in all_regexes {
        let the_regex = Regex::new(pattern).unwrap();

        text = the_regex.replace_all(&text, replace).to_string();
    }

    parse_math(&text)
}

struct KatexSeparator {
    start: String,
    end: String,
    block: bool,
}

fn parse_math(text: &str) -> Result<String, Box<dyn Error>> {
    // God, I hope this isn't complicated...
    let separators = vec![
        KatexSeparator { start: r"\[".into(), end: r"\]".into(), block: true },
        KatexSeparator { start: r"\(".into(), end: r"\)".into(), block: false },
        KatexSeparator { start: "$$".into(), end: "$$".into(), block: true },
        KatexSeparator { start: "$".into(), end: "$".into(), block: false },
    ];

    let mut output_text = String::new();
    let mut remaining_text = text;
    loop {
        let next_separator = separators
            .iter()
            .filter(|sep| find_separator(remaining_text, sep).is_some())
            .min_by_key(|sep| find_separator(remaining_text, sep).unwrap());

        if let Some(sep) = next_separator {
            let (before_start, after_start) = remaining_text.split_once(&sep.start).unwrap();

            output_text += before_start;

            let (inside, after_end) = after_start.split_once(&sep.end).unwrap();

            let opts = katex::Opts::builder()
                .display_mode(sep.block)
                .build()?;

            let output_math = katex::render_with_opts(inside, &opts)?;

            output_text += &output_math;
            remaining_text = after_end;
        } else {
            output_text += remaining_text;
            break;
        }
    }

    Ok(output_text)
}

fn find_separator(text: &str, sep: &KatexSeparator) -> Option<usize> {
    if let Some((before_start, after_start)) = text.split_once(&sep.start) {
        if after_start.contains(&sep.end) {
            Some(before_start.len())
        } else { None }
    } else { None }
}

impl HtmlHandler<io::Error> for SkinsuitHtmlHandler {
    fn start<W: io::Write>(&mut self, mut w: W, element: &Element) -> Result<(), io::Error> {
        match element {
            // Noop to ignore the root-level block tags.
            Element::Document { .. } => Ok(()),

            // Heading.
            // h1 is reserved for the site title, and h2 for the article title, so
            // an article-level heading should start with h3.
            Element::Title(orgize::elements::Title { level, .. }) => {
                if level > &4 {
                    Err(io::Error::new(io::ErrorKind::InvalidInput, "invalid heading level"))
                } else {
                    let new_level = level + 2;
                    write!(w, "<h{}>", new_level)
                }
            }

            // Tufte-style sidenote.
            Element::FnRef(note) => {
                let text = note.definition.clone().unwrap().into_owned();

                let label = if !note.label.is_empty() {
                    note.label.clone().into_owned()
                } else {
                    slugify!(&text)
                };

                write!(
                    w,
                    "<label for=\"sidenote-{0}\" class=\"margin-toggle sidenote-number\"></label>
                    <input type=\"checkbox\" id=\"sidenote-{0}\" class=\"margin-toggle\" />
                    <span class=\"sidenote\">",
                    label,
                )?;

                let org = Org::parse_string(text);
                let mut handler = InlineHtmlHandler::default();

                // TODO Nest these fuckers!
                org.write_html_custom(w, &mut handler)
            }
            Element::Text { value } => {
                let processed = process_text(value)
                    .map_err(|err| io::Error::new(io::ErrorKind::InvalidInput, err.to_string()))?;

                write!(w, "{}", processed)
            }
            Element::Link(orgize::elements::Link { path, desc }) => {
                if path.ends_with(".jpg") {
                    let description = desc.clone().unwrap_or_default().into_owned();

                    let processed = process_text(&description)
                        .map_err(|err| io::Error::new(io::ErrorKind::InvalidInput, err.to_string()))?;

                    write!(w, "<img src=\"{}\">{}</a>", path, processed)
                } else {
                    let description = desc.clone().unwrap_or_default().into_owned();

                    let processed = process_text(&description)
                        .map_err(|err| io::Error::new(io::ErrorKind::InvalidInput, err.to_string()))?;

                    write!(w, "<a href=\"{}\">{}</a>", path, processed)
                }
            }

            // Syntax highlighting for code.
            Element::SourceBlock(orgize::elements::SourceBlock {
                contents, language, ..
            }) => {
                let noweb_regex = Regex::new(r"(?m)^\s*<<.*>>\s*$").unwrap();
                let contents_processed = noweb_regex.replace_all(contents.as_ref(), "");

                let syntax_set = SyntaxSet::load_defaults_newlines();

                let syntax = syntax_set.find_syntax_by_token(language.as_ref()).unwrap();

                let mut html_generator = ClassedHTMLGenerator::new_with_class_style(
                    syntax,
                    &syntax_set,
                    ClassStyle::Spaced,
                );

                for line in LinesWithEndings::from(&contents_processed) {
                    let _ = html_generator.parse_html_for_line_which_includes_newline(line);
                }

                let output_html = html_generator.finalize();

                write!(w, "<pre class=\"code\"><code>")?;
                write!(w, "{}", output_html)
            }

            // Summary/details element for org-babel output.
            Element::ExampleBlock(orgize::elements::ExampleBlock{
                contents, ..
            }) => {
                let lines = contents
                    .chars()
                    .filter(|&c| c == '\n')
                    .count();

                write!(w, "<details class=\"babel-output\">")?;
                write!(w, "<summary>{} lines of output</summary>", lines)?;

                write!(w, "<pre class=\"example\">")?;
                write!(w, "{}", contents)
            }

            Element::Rule(_) => {
                write!(w, "<div class=\"hr\"><hr /></div>")
            }

            _ => self.0.start(w, element),
        }
    }

    fn end<W: io::Write>(&mut self, mut w: W, element: &Element) -> Result<(), io::Error> {
        // These are all just the closing tags. Nothing special.
        match element {
            Element::Document { .. } => {
                write!(w, "")
            }
            Element::Title(orgize::elements::Title { level, .. }) => {
                let new_level = level + 2;
                write!(w, "</h{}>", new_level)
            }
            Element::FnRef(_) => {
                write!(w, "</span>")
            }
            Element::SourceBlock { .. } => {
                write!(w, "</code></pre>")
            }
            Element::ExampleBlock { .. } => {
                write!(w, "</pre></details>")
            }
            _ => self.0.end(w, element),
        }
    }
}

/// Handler for inline HTML, ie inside footnote references and other places where
/// block-level elements are not wanted.
#[derive(Default)]
struct InlineHtmlHandler(DefaultHtmlHandler);

impl HtmlHandler<io::Error> for InlineHtmlHandler {
    fn start<W: io::Write>(&mut self, w: W, element: &Element) -> Result<(), io::Error> {
        match element {
            Element::Document { .. } => Ok(()),
            Element::Section {} => Ok(()),
            Element::Paragraph { .. } => Ok(()),
            _ => self.0.start(w, element),
        }
    }

    fn end<W: io::Write>(&mut self, w: W, element: &Element) -> Result<(), io::Error> {
        match element {
            Element::Document { .. } => Ok(()),
            Element::Section {} => Ok(()),
            Element::Paragraph { .. } => Ok(()),
            _ => self.0.end(w, element),
        }
    }
}
