use std::collections::HashMap;

use itertools::Itertools;
use maud::{html, Markup, PreEscaped, DOCTYPE};

use crate::templates::layout::{smart_quotes, Context, Head, Layout};
use crate::Config;

pub struct ThoughtLayout {
    pub config: Config,
    pub css_hash: String,
    pub hot_reload_websocket_port: Option<u16>,
}

impl ThoughtLayout {
    pub fn new(
        config: Config,
        css_hash: impl Into<String>,
        hot_reload_websocket_port: Option<u16>,
    ) -> Self {
        Self {
            config,
            css_hash: css_hash.into(),
            hot_reload_websocket_port,
        }
    }

    fn html_head(&self, head: &Head) -> Markup {
        let the_title = head.title.clone().unwrap_or(self.config.title.clone());

        html! {
            head {
                title { (the_title) };

                meta charset="utf8";

                meta name="title" content=(the_title);

                @if let Some(ref description) = head.description {
                    meta name="description" content=(smart_quotes(description.clone()));
                };

                meta name="author" content="Meatless Trainer";
                meta name="viewport" content="width=device-width,initial-scale=1";

                link rel="sitemap" href="/sitemap.xml";

                link rel="stylesheet" href=(format!("/css/thoughts.css?hash={}", self.css_hash));
                link rel="stylesheet" href="/css/code.css";

                link rel="me" href="mailto:mt@skinsuit.systems";
                link rel="webmention" href="https://webmention.io/skinsuit.systems/webmention";

                link rel="icon" href="/favicon.ico";

                @if let Some(port) = self.hot_reload_websocket_port {
                    script {
                        // TODO Set license type in config.
                        "/* @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0 */"
                        (format!("const port = {}", port))
                        (PreEscaped(r#"
                            const socket = new WebSocket("ws://localhost:" + port);
                            socket.addEventListener("message", (event) => {
                                console.log(event);
                                window.location.reload();
                            })
                        "#))
                        "/* @license-end */"
                    }
                }
            };
        }
    }

    fn header(&self, _head: &Head) -> Markup {
        let display_title = "🦊🚲 kitted-out kit";

        html! {
            header.header {
                a.skip-link href="#main" { "Skip to content" };
                h1 {
                    a href="/thoughts" { (display_title) };
                }
            };
        }
    }

    fn main(&self, head: &Head, content: &Markup) -> Markup {
        html! {
            main {
                article {
                    @if let Some(ref title) = head.title {
                        h2 { (smart_quotes(title.clone())) };

                        @let date = format!("{}", head.time.format("%d %b %Y"));
                        div .meta { (date) }
                    }

                    (content)
                };
            };
        }
    }

    fn footer(&self) -> Markup {
        html! {
            footer {
            };
        }
    }
}

impl Layout for ThoughtLayout {
    fn render(&self, context: Context) -> Markup {
        let head = context.head;
        let content = context.content;
        let _options = context.options;

        html! {
            (DOCTYPE)

            html lang="en" {
                (self.html_head(&head))

                body {
                    (self.header(&head))
                    (self.main(&head, &content))
                    (self.footer())
                };
            }
        }
    }
}

pub struct ThoughtIndexLayout {
    pub config: Config,
    pub css_hash: String,
    pub hot_reload_websocket_port: Option<u16>,
    pub sitemap: HashMap<String, Vec<Head>>,
}

impl ThoughtIndexLayout {
    pub fn new(
        config: Config,
        css_hash: impl Into<String>,
        hot_reload_websocket_port: Option<u16>,
        sitemap: &HashMap<String, Vec<Head>>,
    ) -> Self {
        Self {
            config,
            css_hash: css_hash.into(),
            hot_reload_websocket_port,
            sitemap: sitemap.to_owned(),
        }
    }

    fn main(&self, head: &Head, content: &Markup) -> Markup {
        let sorted_sections = self.sitemap.iter().sorted_by_key(|(k, _)| *k);
        html! {
            main {
                article {
                    @if let Some(ref title) = head.title {
                        h2 { (smart_quotes(title.clone())) };

                        @let date = format!("{}", head.time.format("%d %b %Y"));
                        div .meta { (date) }
                    }

                    (content)
                }

                nav {
                    @for (_title, pages) in sorted_sections {
                        section {
                            ul {
                                @for page in pages.iter().sorted_by_key(|p| p.time).rev().take(10) {
                                    @let url_string = page.url.clone().unwrap_or("/".into());
                                    li {
                                        @let date = format!("{}", page.time.format("%d %b %Y"));
                                        time { (date) };
                                        " — "
                                        a href=(url_string) { (page.title.clone().unwrap_or("A blog".into())) };
                                    }
                                };
                            }
                        };
                    };
                };
            }
        }
    }
}

impl Layout for ThoughtIndexLayout {
    fn render(&self, context: Context) -> Markup {
        let head = context.head;
        let content = context.content;
        let _options = context.options;

        let thought_layout = ThoughtLayout::new(
            self.config.clone(),
            self.css_hash.clone(),
            self.hot_reload_websocket_port,
        );

        html! {
            (DOCTYPE)

            html lang="en" {
                (thought_layout.html_head(&head))

                body {
                    (thought_layout.header(&head))
                    (self.main(&head, &content))
                    (thought_layout.footer())
                };
            }
        }
    }
}
