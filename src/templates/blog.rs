use maud::{html, Markup, PreEscaped, DOCTYPE};

use itertools::Itertools;

use crate::Config;
use crate::templates::layout::{Context, Head, Layout, Sitemap, smart_quotes};

pub struct BlogLayout {
    pub config: Config,
    pub css_hash: String,
    pub hot_reload_websocket_port: Option<u16>,
}

impl BlogLayout {
    pub fn new(
        config: Config,
        css_hash: impl Into<String>,
        hot_reload_websocket_port: Option<u16>,
    ) -> Self {
        Self {
            config,
            css_hash: css_hash.into(),
            hot_reload_websocket_port,
        }
    }

    fn html_head(&self, head: &Head) -> Markup {
        let the_title = head.title.clone().unwrap_or(self.config.title.clone());

        html! {
            head {
                title { (the_title) };

                meta charset="utf8";

                meta name="title" content=(the_title);

                @if let Some(ref description) = head.description {
                    meta name="description" content=(smart_quotes(description.clone()));
                };

                meta name="author" content="Meatless Trainer";
                meta name="viewport" content="width=device-width,initial-scale=1";

                link rel="sitemap" href="/sitemap.xml";

                link rel="stylesheet" href=(format!("/css/main.css?hash={}", self.css_hash));
                link rel="stylesheet" href="/css/code.css";

                link rel="me" href="mailto:mt@skinsuit.systems";
                link rel="webmention" href="https://webmention.io/skinsuit.systems/webmention";

                link rel="icon" href="/favicon.ico";

                @if let Some(port) = self.hot_reload_websocket_port {
                    script {
                        // TODO Set license type in config.
                        "/* @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0 */"
                        (format!("const port = {}", port))
                        (PreEscaped(r#"
                            const socket = new WebSocket("ws://localhost:" + port);
                            socket.addEventListener("message", (event) => {
                                console.log(event);
                                window.location.reload();
                            })
                        "#))
                        "/* @license-end */"
                    }
                }
            };
        }
    }

    fn header(&self, _head: &Head) -> Markup {
        let display_title = self
            .config
            .display_title
            .clone()
            .unwrap_or(self.config.title.clone());

        html! {
            header.header {
                a.skip-link href="#main" { "Skip to content" };
                img src="/favicon.ico";
                h1 {
                    a href="/blog" { (display_title) };
                }
            };
        }
    }

    fn nav(&self, sitemap: &Sitemap) -> Markup {
        let sorted_sections = sitemap.iter().sorted_by_key(|(k, _)| *k);

        html! {
            nav {
                @for (title, pages) in sorted_sections {
                    section {
                        h3 { (title.clone()) };
                        ul {
                            @for page in pages.iter().sorted_by_key(|p| p.time).rev().take(10) {
                                @let url_string = page.url.clone().unwrap_or("/".into());
                                li { a href=(url_string) { (page.title.clone().unwrap_or("A blog".into())) } }
                            };
                        }
                    };
                };
            };
        }
    }

    fn main(&self, head: &Head, content: &Markup) -> Markup {
        html! {
            main {
                article {
                    @if let Some(ref title) = head.title {
                        h2 { (smart_quotes(title.clone())) };

                        @let date = format!("{}", head.time.format("%d %b %Y"));
                        div .meta { (date) }
                        @if !head.revs.is_empty() {
                            details {
                                summary { "See revisions..." }
                                ol {
                                    @for rev in &head.revs {
                                        li {
                                            strong {  (rev.date.format("%d %b %Y")) }
                                            ("—")
                                            (rev.message)
                                        }
                                    }
                                }
                            }
                        }
                    }

                    @if let Some(true) = head.math {
                        div.math-alert {
                            span.dangerous-bend { "☡" }
                            div {
                                p {
                                    "This page contains "
                                    b { "mathematical expressions" }
                                    " written with MathML."
                                    " If they don't display properly, your system may have missing or misconfigured mathematical fonts. To remedy this, I recommend following "
                                        a href="https://developer.mozilla.org/en-US/docs/Web/MathML/Fonts" { "MDN's tutorial."}
                                }
                                p {
                                    "If you're using Tor Browser, you might have MathML disabled. A legacy version of this page that falls back to PNGs is in development."
                                }
                            }
                        }
                    }

                    (content)
                };
            };
        }
    }

    fn footer(&self) -> Markup {
        html! {
            footer {
            };
        }
    }
}

impl Layout for BlogLayout {
    fn render(&self, context: Context) -> Markup {
        let head = context.head;
        let sitemap = context.sitemap;
        let content = context.content;
        let _options = context.options;

        html! {
            (DOCTYPE)

            html lang="en" {
                (self.html_head(&head))

                body {
                    (self.header(&head))
                    (self.nav(sitemap))
                    (self.main(&head, &content))
                    (self.footer())
                };
            }
        }
    }
}
