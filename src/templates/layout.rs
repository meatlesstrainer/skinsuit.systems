use std::collections::HashMap;
use std::error::Error;
use std::path::Path;

use crowbook_text_processing::clean;
use maud::Markup;

use chrono::{DateTime, NaiveDate, Utc};

use orgize::Org;
use std::fs;

#[derive(Clone, Debug, Default)]
pub struct Head {
    pub title: Option<String>,
    pub description: Option<String>,
    pub url: Option<String>,
    pub tags: Vec<String>,
    pub time: DateTime<Utc>,
    pub math: Option<bool>,
    pub revs: Vec<Revision>,
}

#[derive(Clone, Debug, Default)]
pub struct Revision {
    pub date: DateTime<Utc>,
    pub message: String,
}

impl TryFrom<String> for Revision {
    type Error = Box<dyn Error>;
    fn try_from(source: String) -> Result<Self, Self::Error> {
        let (date_str, message) = source.split_once(' ').ok_or("Invalid rev")?;

        let date = NaiveDate::parse_from_str(date_str, "%Y-%m-%d")?
            .and_hms_opt(0, 0, 0)
            .unwrap()
            .and_utc();

        Ok(Revision {
            date,
            message: message.into(),
        })
    }
}

pub type Sitemap = HashMap<String, Vec<Head>>;

pub struct Context<'a> {
    pub head: Head,
    pub content: Markup,
    pub sitemap: &'a Sitemap,
    pub options: Option<Options>,
}

#[derive(Debug, Default)]
pub struct Options {
    pub is_index: bool,
    pub full_width: bool,
    pub is_thought: bool,
}

impl Head {
    pub fn new<P, F>(path: P, base_dir: F) -> Result<Head, Box<dyn Error>>
    where
        P: AsRef<Path> + Clone,
        F: AsRef<Path> + Clone,
    {
        let org_content = fs::read_to_string(path.clone())?;
        let markup = Org::parse(&org_content);

        let the_path: &Path = path.as_ref();

        let the_base_dir: &Path = base_dir.as_ref();

        let filename = the_base_dir.join(the_path.file_stem().unwrap());
        let out_path = filename.to_str().unwrap();

        // let out_path = the_path.file_stem().unwrap().to_str().unwrap();

        let keywords: HashMap<String, String> = markup
            .keywords()
            .map(|kw| {
                (
                    kw.key.clone().into_owned().to_lowercase(),
                    kw.value.clone().into_owned(),
                )
            })
            .collect();

        let tags: Vec<String> = keywords
            .get("tags")
            .unwrap_or(&"".to_string())
            .split(',')
            .map(|s| s.to_string())
            .collect();

        let date_str: String = keywords
            .get("date")
            .unwrap_or(&"2020-01-01".to_string())
            .to_string();

        let date = NaiveDate::parse_from_str(&date_str, "%Y-%m-%d")?
            .and_hms_opt(0, 0, 0)
            .unwrap()
            .and_utc();

        let mut revs: Vec<Revision> = markup
            .keywords()
            .filter(|k| k.key.to_lowercase() == "revs")
            .map(|k| k.value.clone().into_owned())
            .map(Revision::try_from)
            .collect::<Result<Vec<Revision>, Box<dyn Error>>>()?;

        revs.sort_by_key(|k| k.date);

        //TODO Fix this, it shouldn't be an Option.
        let math = keywords.get("math").is_some_and(|b| !b.is_empty());

        Ok(Head {
            title: keywords.get("title").cloned(),
            description: keywords
                .get("description")
                .or(keywords.get("subtitle"))
                .cloned(),
            url: Some(out_path.into()),
            tags: tags.clone(),
            time: date,
            math: Some(math),
            revs,
        })
    }
}

pub fn smart_quotes(text: impl Into<String>) -> String {
    clean::quotes(text.into()).to_string()
}

pub trait Layout {
    fn render(&self, context: Context) -> Markup;
}
