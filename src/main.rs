use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::io;
use std::path::Path;

use clap::Parser;
use maud::PreEscaped;
use orgize::Org;
use serde::Deserialize;
use syntect::highlighting::ThemeSet;
use syntect::html::{css_for_theme_with_class_style, ClassStyle};

mod templates;
use templates::blog::BlogLayout;
use templates::layout::{Context, Head, Layout};
use templates::thought::{ThoughtIndexLayout, ThoughtLayout};

mod handlers;
use handlers::SkinsuitHtmlHandler;

// Conditional compilation --- goes with cargo stuff
#[cfg(feature = "watch")]
mod watch;

#[derive(Debug, Parser)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Parser)]
enum Commands {
    #[clap(name = "build")]
    Build {
        #[clap(long)]
        websocket_port: Option<u16>,
    },
    #[cfg(feature = "watch")]
    #[clap(name = "watch")]
    Watch,
}

#[derive(Clone, Deserialize)]
pub struct Config {
    pub title: String,
    pub display_title: Option<String>,
    pub date_format: String,
    pub sections: Vec<Section>,
    pub css_theme: Option<String>,
}

#[derive(Clone, Deserialize)]
pub struct Section {
    pub name: String,
    pub tags: Option<Vec<String>>,
    pub fallback: Option<bool>,
}

fn get_config() -> Result<Config, Box<dyn Error>> {
    let config_file = fs::read_to_string("src/config.toml")?;

    let config: Config = toml::from_str(&config_file)?;

    Ok(config)
}

fn get_css(config: Config) -> Result<String, Box<dyn Error>> {
    let theme_name = config.css_theme.unwrap_or("InspiredGitHub".to_string());

    let themes = ThemeSet::load_defaults().themes;

    let theme = themes.get(&theme_name).ok_or("bad theme name")?;

    Ok(css_for_theme_with_class_style(theme, ClassStyle::Spaced)?)
}

fn build_sitemap<F>(base_dir: F) -> Result<HashMap<String, Vec<Head>>, Box<dyn Error>>
where
    F: AsRef<Path> + Clone,
{
    let mut sitemap = HashMap::new();

    let articles = fs::read_dir(base_dir.clone())?;

    let config = get_config()?;
    let sections = config.sections;

    for entry in articles {
        let entry = entry?;

        let path = entry.path();

        let filename = path.file_stem().unwrap().to_str().unwrap();

        if filename == "index" {
            continue;
        }

        let head = Head::new(path, base_dir.clone())?;

        let mut category_titles: Vec<String> = sections
            .iter()
            .filter_map(|section| {
                let name = section.name.clone();

                section
                    .tags
                    .clone()
                    .unwrap_or_default()
                    .iter()
                    .find(|tag| head.tags.contains(tag))
                    .and(Some(name))
            })
            .collect();

        if category_titles.is_empty() {
            let default = sections.iter().find_map(|sec| {
                let is_fallback = sec.fallback.unwrap_or(false);
                if is_fallback {
                    Some(sec.name.clone())
                } else {
                    None
                }
            });

            if let Some(title) = default {
                category_titles.push(title);
            }
        }

        for title in category_titles {
            sitemap
                .entry(title.to_string())
                .or_insert_with(Vec::new)
                .push(head.clone())
        }
    }

    Ok(sitemap)
}

fn build(websocket_port: Option<u16>) -> Result<(), Box<dyn Error>> {
    // Create `dist/` if it doesn't exist.
    fs::create_dir_all("dist")?;

    // Leave the Git stuff alone.
    for entry in (fs::read_dir("dist")?).flatten() {
        let path = entry.path();
        if path.is_dir() {
            if let Some(os_str) = path.file_name() {
                if os_str.to_str() == Some(".git") {
                    continue;
                }
            }
            fs::remove_dir_all(path).ok();
        } else if path.is_file() {
            fs::remove_file(path).ok();
        }
    }

    copy_dir("static", "dist")?;

    let config = get_config()?;
    let css = get_css(config.clone())?;

    fs::write("dist/css/code.css", css)?;

    let sitemap = build_sitemap("content/")?;

    fs::create_dir_all("dist/blog/")?;
    let articles = fs::read_dir("content/")?;
    for entry in articles {
        let layout = BlogLayout::new(config.clone(), "", websocket_port);
        let entry = entry?;
        let path = entry.path();
        let filename = path.file_stem().unwrap().to_str().unwrap();

        if filename != "index" {
            fs::create_dir_all(format!("dist/blog/{}", filename))?;
            build_page(
                path.clone(),
                format!("dist/blog/{}/index.html", filename),
                "/blog".into(),
                layout,
                &sitemap,
            )?;
        }
    }

    let layout = BlogLayout::new(config.clone(), "", websocket_port);
    build_page(
        "content/index.org",
        "dist/blog/index.html",
        "/thoughts",
        layout,
        &sitemap,
    )?;

    fs::create_dir_all("dist/thoughts/")?;
    let articles = fs::read_dir("thoughts/")?;
    for entry in articles {
        let layout = ThoughtLayout::new(config.clone(), "", websocket_port);
        let entry = entry?;
        let path = entry.path();
        let filename = path.file_stem().unwrap().to_str().unwrap();

        if filename != "index" {
            fs::create_dir_all(format!("dist/thoughts/{}", filename))?;
            build_page(
                path.clone(),
                format!("dist/thoughts/{}/index.html", filename),
                "/thoughts".into(),
                layout,
                &sitemap,
            )?;
        }
    }

    let thought_sitemap = build_sitemap("thoughts/")?;

    let layout = ThoughtIndexLayout::new(config, "", websocket_port, &thought_sitemap);
    build_page(
        "thoughts/index.org",
        "dist/thoughts/index.html",
        "/thoughts",
        layout,
        &thought_sitemap,
    )?;

    Ok(())
}

fn build_page<F, T>(
    from: F,
    to: T,
    base_dir: F,
    layout: impl Layout,
    sitemap: &HashMap<String, Vec<Head>>,
) -> Result<(), Box<dyn Error>>
where
    F: AsRef<Path> + Send + Sync + Clone,
    T: AsRef<Path> + Send,
{
    let org_content = fs::read_to_string(from.clone())?;
    let markup = Org::parse(&org_content);

    let head = Head::new(from, base_dir)?;

    let mut writer = Vec::new();
    let mut handler = SkinsuitHtmlHandler::default();
    markup.write_html_custom(&mut writer, &mut handler)?;
    let html_content = PreEscaped(String::from_utf8(writer)?);

    let context = Context {
        head,
        content: html_content,
        sitemap,
        options: None,
    };

    fs::write(to, layout.render(context).into_string())?;

    Ok(())
}

fn copy_dir<F, T>(from: F, to: T) -> io::Result<()>
where
    F: AsRef<Path> + Send + Sync,
    T: AsRef<Path> + Send,
{
    fs::read_dir(&from)?
        .map_while(|item| item.ok())
        .try_for_each(|entry| {
            let filename = entry.file_name();
            let old_path = entry.path();

            let new_path = to.as_ref().join(filename);
            if new_path.exists() {
                return Err(io::Error::new(io::ErrorKind::AlreadyExists, "file exists"));
            }

            if old_path.is_dir() {
                fs::create_dir(&new_path)?;
                copy_dir(old_path, &new_path)
            } else {
                fs::copy(old_path, new_path).map(|_| ())
            }
        })
}

fn main() -> Result<(), Box<dyn Error>> {
    let cli = Cli::parse();

    match cli.command {
        Commands::Build { websocket_port } => build(websocket_port),
        #[cfg(feature = "watch")]
        Commands::Watch => watch::watch(),
    }
}
