#+title:🐉 Draconic numbers
#+date: 2024-01-01
#+tags:math,programming
#+math:t

The didactic voice of this post was inspired by the incomparable Clifford Pickover, who started my journey down this rabbit hole in the first place!

For more on Lisky and Sevvy, see [[/blog/characters][/characters]].

-------

Chicago, New Year's Day. L&S are identically kitted out, having dinner in a lakeside Asian joint in Dearborn Park after a long ride together.

*Lisky:* Happy new year, Sevvy. Here's to 2024.

*Sevvy:* Cheers.

*L:* I rather enjoy this place's "dragon salad." It's a somewhat worrying name, but it's just tempura tofu and greens.

*S:* Nice that it's vegan.

*L:* And it's to ring in the Year of the Dragon, too, I guess. What a coincidence.

*S:* /Lunar/ new year isn't for a month, though.

*L:* It's a segue; indulge me. I discovered something about 2024 --- the number --- today.

L passes S a red envelope on which is written the following factorization:

$$\begin{align}
2024 &= 2 \cdot 1012 \\
&= 2^2 \cdot 506 \\
&= 2^3 \cdot 253 \\
&= 2^3 \cdot 11 \cdot 23.
\end{align}$$

*L:* Do you notice anything?

*S:* Factors of both $2^3$ and $23$. That's neat.

*L:* /(smiling)/ Indeed! In fact, the final factorization contains only the digits $\{1, 2, 3\}$ exactly twice each!

*S:* You're curious about the other numbers like this.

*L:* Exactly. Since the Year of the Dragon has this property, I call such numbers, whose prime factorization contains each digit equally often, /draconic./  /(Flicks her tongue suggestively.)/ We might call 2024 itself 123-draconic.

*S:* Oh, that's cute. Why don't we go about generating all such numbers?

*L:* I'm so glad you asked, dear.

*S:* First we have to consider the possible prime factors of such a number. This amounts to listing all the primes consisting only of 1, 2, and 3, with no more than two of each:

#+name: prime-factors
#+begin_src python
import itertools
from collections import Counter

def is_prime(n):
    """Trial division."""
    if n < 2:
        return False

    for d in range(2, int(n ** 0.5) + 1):
        if n % d == 0:
            return False

    return True

def all_numbers_containing(digit_counts):
    """Generate the numbers consisting exactly of the numbers in `digit_counts`
    with their associated frequencies, with leading zeroes disallowed.
    """
    all_digits = ''.join(str(d) * c for d, c in digit_counts.items())

    all_numbers = set()

    for num in itertools.permutations(all_digits):
        num = ''.join(num)

        if num and not num.startswith("0"):
            all_numbers.add(int(num))

    return all_numbers


def primes_composed_only_of(digits, n=2):
    """Generate the primes consisting only of the numerals in `digits`
    with no more than `n` copies of each.
    """
    for counts in itertools.product(range(n + 1), repeat=len(digits)):
        digit_counts = Counter(dict(zip(digits, counts)))

        # Skip any arrangements of digits that are divisible by 3
        # but aren't composed of a single 3.
        if sum(d * c for d, c in digit_counts.items()) % 3 == 0 and digit_counts.total() > 1:
            continue

        for num in all_numbers_containing(digit_counts):
            if is_prime(num):
                yield num
#+end_src


#+begin_src python :noweb yes
<<prime-factors>>
test_case = set(primes_composed_only_of([1, 2, 3], n=1))
assert test_case == set((2, 3, 13, 23, 31))

with_two = set(primes_composed_only_of([1, 2, 3], n=2))
return with_two
#+end_src

#+RESULTS:
: {2, 3, 131, 2311, 11, 13, 23311, 31123, 3221, 23, 23321, 31, 12323, 2213, 1321, 3121, 311, 313, 33211, 1213, 32321, 2113, 21313, 1223, 331, 21323, 1231, 211, 2131, 32213, 31321, 23131, 223, 1123, 233, 113, 22133, 31223, 31231}

*L:* /(takes the printout from S's "mouth")/ That's beautiful. I see that no six-digit numbers appear, since they'd necessarily be divisible by 3. Now we need to assemble them into our draconic numbers. So we need a sort of exact-cover thing, don't we?

*S:* Maybe if you're Knuth. Don't see the point in writing DLX just for this. Backtracking search is easier:

#+begin_src python :noweb yes :results output
<<prime-factors>>
from math import log10

def _draconic_search(digits, factors, primes, freqs):
    """Recursive backtracking search to generate draconic factorizations.
    """
    # Halt if any numbers have been used more than allowed.
    if freqs and min(freqs.values()) < 0:
        return

    # Yield solution and halt if all number allowances have been met.
    if freqs.total() == 0:
        yield factors
        return

    for base in primes:
        # Force bases to be sorted in increasing order to prevent duplication.
        if any(factor >= base for factor in factors):
            continue

        remaining_freqs = freqs.copy()
        remaining_freqs.subtract(Counter(str(base)))

        # Handle unwritten exponents of 1 separately.
        new_factors = factors.copy()
        new_factors[base] = 1

        yield from _draconic_search(digits, new_factors, primes, remaining_freqs)

        # Recurse on all possible non-1 exponents.
        for exponent in all_numbers_containing(remaining_freqs):
            if exponent == 1:
                continue

            remaining_freqs_after_exp = remaining_freqs.copy()
            remaining_freqs_after_exp.subtract(Counter(str(exponent)))

            new_factors = factors.copy()
            new_factors[base] = exponent

            yield from _draconic_search(digits, new_factors, primes, remaining_freqs_after_exp)


def draconic_numbers(digits, n=2):
    """Generate the numbers whose prime factorizations contain exactly `n`
    copies of the given `digits`.`
    """
    primes = set(primes_composed_only_of(digits, n))
    initial_counts = Counter({str(d): n for d in digits})

    yield from _draconic_search(digits, Counter(), primes, initial_counts)


def counter_log(counter):
    """Compute the logarithm of the number whose prime factorization is the
    given `counter`.
    """
    return sum(e * log10(b) for (b, e) in counter.items())


if __name__ == "__main__":
    dragons = list(draconic_numbers([1, 2, 3], n=2))
    dragons = list(sorted(dragons, key=counter_log))

    for counter in dragons:
        log_n = counter_log(counter)
        if log_n < 30:
            number = 1
            for (b, e) in counter.items():
                number *= (b ** e)
            number = str(number)
        else:
            # Write big numbers in scientific notation.
            (characteristic, mantissa) = divmod(log_n, 1.0)
            significand = 10 ** mantissa
            characteristic = int(characteristic)
            number = f"{significand:.3f}e+{characteristic}"

        factorization = " * ".join(f"{b}^{e}" if e > 1 else str(b) for (b, e) in counter.items())

        print(factorization, "=", number)
#+end_src

#+RESULTS:
#+begin_example
2 * 3 * 11 * 23 = 1518
2 * 11 * 233 = 5126
2 * 23 * 113 = 5198
2 * 23 * 131 = 6026
2 * 3 * 1123 = 6738
2 * 3 * 1213 = 7278
3 * 11 * 223 = 7359
2 * 3 * 1231 = 7386
2 * 3 * 1321 = 7926
2 * 3 * 2113 = 12678
2 * 3 * 2131 = 12786
2 * 3 * 2311 = 13866
2 * 23 * 311 = 14306
3 * 23 * 211 = 14559
13 * 1223 = 15899
3 * 11 * 23^2 = 17457
2 * 3 * 3121 = 18726
2 * 13 * 31^2 = 24986
113 * 223 = 25199
23 * 1123 = 25829
23 * 1213 = 27899
23 * 1231 = 28313
13 * 2213 = 28769
131 * 223 = 29213
23 * 1321 = 30383
31 * 1223 = 37913
13 * 3221 = 41873
2 * 21313 = 42626
2 * 23131 = 46262
2 * 23311 = 46622
23 * 2113 = 48599
23 * 2131 = 49013
211 * 233 = 49163
23 * 2311 = 53153
2 * 31123 = 62246
2 * 31231 = 62462
2 * 31321 = 62642
2 * 33211 = 66422
31 * 2213 = 68603
223 * 311 = 69353
23 * 3121 = 71783
2 * 3 * 113^2 = 76614
31 * 3221 = 99851
2 * 3 * 131^2 = 102966
2 * 11 * 23^3 = 267674
23 * 113^2 = 293687
23 * 131^2 = 394703
2 * 3 * 311^2 = 580326
11 * 233^2 = 597179
23 * 311^2 = 2224583
3 * 1123^2 = 3783387
3 * 1213^2 = 4414107
3 * 1231^2 = 4546083
3 * 1321^2 = 5235123
3 * 2113^2 = 13394307
3 * 2131^2 = 13623483
3 * 2311^2 = 16022163
3 * 3121^2 = 29221923
2 * 3 * 211^3 = 56363586
11 * 223^3 = 121985237
23 * 211^3 = 216060413
21313^2 = 454243969
23131^2 = 535043161
23311^2 = 543402721
31123^2 = 968641129
31231^2 = 975375361
31321^2 = 981005041
33211^2 = 1102970521
2 * 1123^3 = 2832495734
2 * 1213^3 = 3569541194
2 * 1231^3 = 3730818782
2 * 1321^3 = 4610398322
2 * 2113^3 = 18868113794
2 * 2131^3 = 19354428182
2 * 2311^3 = 24684812462
2 * 3121^3 = 60801081122
2 * 3 * 13^12 = 139788510734886
2 * 3 * 23^11 = 5716858547483562
13 * 23^12 = 284890117616264173
2 * 3 * 31^12 = 4725976702731298566
23 * 31^12 = 18116244027136644503
2 * 3 * 13^21 = 1482387174440702356226478
2 * 3 * 11^23 = 5372581459531424233479186
3 * 223^11 = 203457989317852009699625181
2 * 233^11 = 219758219102620905024229234
13 * 23^21 = 513130593569041316534244665099
2 * 313^12 = 1.768e+30
2 * 331^12 = 3.459e+30
2 * 3 * 31^21 = 1.250e+32
11 * 23^23 = 2.297e+32
23 * 31^21 = 4.790e+32
13 * 31^22 = 8.393e+33
2 * 3 * 11^32 = 1.267e+34
1223^13 = 1.369e+40
2213^13 = 3.053e+43
11 * 23^32 = 4.137e+44
3221^13 = 4.017e+45
3 * 113^22 = 4.414e+45
3 * 131^22 = 1.140e+47
2 * 113^23 = 3.325e+47
2 * 131^23 = 9.960e+48
2 * 313^21 = 5.099e+52
2 * 331^21 = 1.650e+53
3 * 211^23 = 8.622e+53
3 * 311^22 = 2.079e+55
2 * 311^23 = 4.310e+57
2 * 113^32 = 9.989e+65
2 * 131^32 = 1.132e+68
1123^23 = 1.441e+70
1213^23 = 8.488e+70
1231^23 = 1.191e+71
1321^23 = 6.036e+71
3 * 211^32 = 7.148e+74
2113^23 = 2.970e+76
2131^23 = 3.609e+76
2 * 211^33 = 1.005e+77
2311^23 = 2.330e+77
2 * 311^32 = 1.173e+80
3121^23 = 2.338e+80
1223^31 = 5.131e+95
1123^32 = 4.094e+97
1213^32 = 4.826e+98
1231^32 = 7.731e+98
1321^32 = 7.394e+99
2213^31 = 4.948e+103
2113^32 = 2.493e+106
2131^32 = 3.271e+106
2311^32 = 4.381e+107
3221^31 = 5.594e+108
3121^32 = 6.568e+111
3 * 13^122 = 2.389e+136
2 * 13^123 = 2.070e+137
2 * 13^132 = 2.196e+147
3 * 23^112 = 9.787e+152
2 * 23^113 = 1.501e+154
3 * 23^121 = 1.763e+165
2 * 23^131 = 4.868e+178
3 * 31^122 = 2.650e+182
2 * 31^123 = 5.477e+183
2 * 31^132 = 1.448e+197
3 * 11^223 = 5.101e+232
3 * 13^212 = 4.296e+236
2 * 13^213 = 3.724e+237
3 * 11^232 = 1.203e+242
2 * 11^233 = 8.821e+242
3 * 13^221 = 4.556e+246
2 * 13^231 = 4.187e+257
233^112 = 1.393e+265
223^113 = 2.283e+265
233^121 = 2.819e+286
3 * 23^211 = 6.334e+287
313^122 = 2.860e+304
331^122 = 2.624e+307
223^131 = 4.246e+307
3 * 31^212 = 4.424e+316
2 * 31^213 = 9.143e+317
3 * 31^221 = 1.170e+330
3 * 11^322 = 6.391e+335
2 * 11^323 = 4.687e+336
2 * 31^231 = 6.391e+344
2 * 11^332 = 1.105e+346
2 * 13^312 = 7.102e+347
2 * 13^321 = 7.531e+357
2 * 23^311 = 6.286e+423
113^223 = 6.863e+457
2 * 31^312 = 4.035e+465
131^223 = 1.417e+472
113^232 = 2.062e+476
2 * 31^321 = 1.067e+479
131^232 = 1.610e+491
233^211 = 3.252e+499
313^212 = 1.136e+529
331^212 = 1.598e+534
2 * 3^1123 = 1.283e+536
2 * 3^1132 = 2.525e+540
211^233 = 3.613e+541
313^221 = 3.276e+551
311^223 = 7.684e+555
331^221 = 7.621e+556
311^232 = 2.091e+578
2 * 3^1213 = 1.120e+579
2 * 3^1231 = 4.338e+587
2 * 3^1312 = 1.924e+626
2 * 3^1321 = 3.786e+630
113^322 = 1.234e+661
131^322 = 5.772e+681
223^311 = 2.103e+730
211^323 = 5.536e+750
211^332 = 4.590e+771
311^322 = 4.665e+802
2 * 3^2113 = 2.872e+1008
2 * 3^2131 = 1.113e+1017
2 * 3^2311 = 8.477e+1102
13^1223 = 2.253e+1362
13^1232 = 2.389e+1372
13^1322 = 4.296e+1472
2 * 3^3112 = 1.266e+1485
2 * 3^3121 = 2.492e+1489
23^1123 = 1.661e+1529
2 * 3^3211 = 2.175e+1532
23^1132 = 2.992e+1541
23^1213 = 5.968e+1651
23^1231 = 1.936e+1676
23^1312 = 3.863e+1786
23^1321 = 6.958e+1798
31^1223 = 8.617e+1823
31^1232 = 2.278e+1837
31^1322 = 3.803e+1971
11^2233 = 2.691e+2325
13^2123 = 7.975e+2364
13^2132 = 8.457e+2374
11^2323 = 1.430e+2419
11^2332 = 3.371e+2428
13^2213 = 1.434e+2465
13^2231 = 1.613e+2485
13^2312 = 2.735e+2575
13^2321 = 2.901e+2585
23^2113 = 2.142e+2877
23^2131 = 6.951e+2901
23^2311 = 8.975e+3146
31^2123 = 1.448e+3166
31^2132 = 3.829e+3179
31^2213 = 2.418e+3300
31^2231 = 1.690e+3327
11^3223 = 2.562e+3356
11^3232 = 6.042e+3365
2^11233 = 2.951e+3381
2^11323 = 3.653e+3408
2^11332 = 1.870e+3411
31^2312 = 1.067e+3448
11^3322 = 3.210e+3459
31^2321 = 2.822e+3461
13^3122 = 5.385e+3477
13^3212 = 9.684e+3577
13^3221 = 1.027e+3588
2^12133 = 2.494e+3652
2^12313 = 3.822e+3706
2^12331 = 1.002e+3712
2^13123 = 2.610e+3950
2^13132 = 1.336e+3953
2^13213 = 3.231e+3977
2^13231 = 8.470e+3982
2^13312 = 2.048e+4007
2^13321 = 1.049e+4010
23^3112 = 4.978e+4237
23^3121 = 8.966e+4249
23^3211 = 3.222e+4372
31^3122 = 1.075e+4656
31^3212 = 1.794e+4790
31^3221 = 4.743e+4803
3^11223 = 5.393e+5354
3^11232 = 1.062e+5359
3^11322 = 9.265e+5401
3^12123 = 1.383e+5784
3^12132 = 2.723e+5788
3^12213 = 1.207e+5827
3^12231 = 4.678e+5835
3^12312 = 2.074e+5874
3^12321 = 4.083e+5878
3^13122 = 6.097e+6260
3^13212 = 5.321e+6303
3^13221 = 1.047e+6308
2^21133 = 4.644e+6361
2^21313 = 7.117e+6415
2^21331 = 1.866e+6421
2^23113 = 5.085e+6957
2^23131 = 1.333e+6963
2^23311 = 2.043e+7017
2^31123 = 9.048e+9368
2^31132 = 4.633e+9371
2^31213 = 1.120e+9396
2^31231 = 2.936e+9401
2^31312 = 7.099e+9425
2^31321 = 3.635e+9428
2^32113 = 9.468e+9666
2^32131 = 2.482e+9672
2^32311 = 3.804e+9726
2^33112 = 5.072e+9967
2^33121 = 2.597e+9970
2^33211 = 3.215e+9997
3^21123 = 1.707e+10078
3^21132 = 3.360e+10082
3^21213 = 1.490e+10121
3^21231 = 5.772e+10129
3^21312 = 2.560e+10168
3^21321 = 5.038e+10172
3^22113 = 3.822e+10550
3^22131 = 1.481e+10559
3^22311 = 1.128e+10645
3^23112 = 1.684e+11027
3^23121 = 3.315e+11031
3^23211 = 2.894e+11074
3^31122 = 9.283e+14848
3^31212 = 8.102e+14891
3^31221 = 1.595e+14896
3^32112 = 2.078e+15321
3^32121 = 4.091e+15325
3^32211 = 3.571e+15368
#+end_example

*L*: Very nice. Good Sevvy~ /(The crowbot ruffles its carbon-fiber feathers, making a faint tinkling.)/ I have a few final questions for you:

1. How many /pandigital draconic numbers/ are there? (That is, numbers whose prime factorization contains one of every digit just once.)
2. How are draconic numbers distributed for different digit sets and counts?
3. What can be said about base-$b$ draconic numbers?
4. Is there a well-defined concept of $p$-adic draconic numbers?

(*S*: Would very much like to meet a pan digital dragon.)
